# MyFood I Versão Final

Versão final do  projeto MyFood desenvolvido durante as aulas de desenvolvimento web avançado.

Site com todas as aulas:
https://gilbriatore.gitlab.io/2021/angular/site

Playlist do youtube de todas as aulas: 
https://www.youtube.com/watch?v=fEibky9RDUg&list=PLfqtDthdyWq7xbgrZXhUfxhgd2Ye94xLL

# Configuração inicial
1. Instalar no ambiente de desenvolvimento o Node.js (https://nodejs.org).
2. Fazer o checkout do projeto utilizando: git clone https://gitlab.com/gilbriatore/2021/angular/myfood.git

# Para rodar o servidor de APIs (srv-apis)
1. Executar, na pasta do servidor de APIs (srv-apis), o comando: npm install
2. Com o MongoDb rodando no servidor localhost porta 27017, executar, na pasta do servidor de APIs (srv-apis), o comando: node .\database\carregardb.js ou .\carregardb.bat
3. Executar, na pasta do servidor, o comando: npm start ou .\rodar.bat
4. O servidor será iniciado em `http://localhost:3000/`. 

# Para rodar o servidor web (srv-web)
1. Rodar, na pasta do servidor web (srv-web), o comando: npm install --legacy-peer-deps ou .\install.bat
2. Executar, na pasta do servidor, o comando: npm start ou .\rodar.bat
3. O servidor será iniciado em `http://localhost:4200/`. 
